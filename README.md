## LÉEME

En este repositorio se encuentra el proyecto de prueba **Venados Test**.
El desarrollo de éste se realizó a través del entorno Android Studio en el lenguaje Java.
La implementación sigue el patrón MVP (Model - View - Presenter) para realizar la separación entre las capas de la Vista y la Lógica, obteniendo así una estructura de código más clara.

Los menús que se implementaron son los siguientes:
1. Inicio
2. Estadísticas
3. Jugadores

*Cabe mencionar que debido a un detalle con el endpoint de estadísticas, en la __última__ versión de este proyecto el menú correspondiente no se encuentra funcional del todo*

Adicionalmente, se incluye en */apks/VenadosTest.apk* el archivo de instalación correspondiente a la última compilación del proyecto. Con éste se podrá proceder a su instalación directa como se indica en el apartado de abajo.

---

## Construir proyecto

Para construir el proyecto de la aplicación en Android es necesario contar con el software Android Studio.
A continuación se describen los pasos para la construcción del proyecto:

1- Clonar el repositorio Git de **Venados Test** en la rama **master**

2- Cuando se tenga listo, abrir Android Studio

3- Elegir *Open an existing Android Studio project*

![](https://i.imgur.com/6GR07p1.png)

4- Navegar hasta la ubicación donde se tiene guardado el proyecto

![](https://i.imgur.com/yWvoTb8.png)

5- Una vez abierto y cuando Android Studio finalice de cargar todos los componentes, dirigirse al menú
*Build -> Build Bundle(s)/APK(s) -> Build APK(s)*

![](https://i.imgur.com/fzFQTwx.png)

6- Esperar a que finalice el proceso y en la parte inferior derecha saldra la siguiente notificación. Dar click en **locate**

![](https://i.imgur.com/BE9t01j.png)

7- Finalmente el instalador de la aplicación estará listo para ser transferido a un dispositivo móvil Android e instalarlo (**app-debug.apk**)

![](https://i.imgur.com/DGgCvCW.png)

---

## Instalar la aplicación

La instalación del APK varía dependiendo de la versión de Android que posea el dispositivo. De manera general es necesario:

1. Permitir la instalación de fuentes desconocidas.
2. Transferir el archivo .apk
3. Abrir el navegador de archivos del dispositivo
4. Buscar el .apk e instalarlo

*En algunos casos es necesario desactivar el Play Protect para evitar que bloquee la instalación*