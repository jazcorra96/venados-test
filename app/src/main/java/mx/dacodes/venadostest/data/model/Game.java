package mx.dacodes.venadostest.data.model;

import com.google.gson.annotations.SerializedName;
import com.saber.stickyheader.stickyData.StickyMainData;

import java.util.Calendar;
import java.util.Date;

import mx.dacodes.venadostest.utils.DateUtils;

public class Game implements StickyMainData, Comparable<Game>{
    @SerializedName("local")
    private boolean local;

    @SerializedName("opponent")
    private String opponent;

    @SerializedName("opponent_image")
    private String opponentImgUrl;

    @SerializedName("datetime")
    private String dateTimeStr;

    @SerializedName("league")
    private String league;

    @SerializedName("image")
    private String bannerImgUrl;

    @SerializedName("home_score")
    private int homeScore;

    @SerializedName("away_score")
    private int awayScore;

    private Date date;

    private Calendar calendar;

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public String getOpponentImgUrl() {
        return opponentImgUrl;
    }

    public void setOpponentImgUrl(String opponentImgUrl) {
        this.opponentImgUrl = opponentImgUrl;
    }

    public String getDateTimeStr() {
        return dateTimeStr;
    }

    public void setDateTimeStr(String dateTimeStr) {
        this.dateTimeStr = dateTimeStr;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getBannerImgUrl() {
        return bannerImgUrl;
    }

    public void setBannerImgUrl(String bannerImgUrl) {
        this.bannerImgUrl = bannerImgUrl;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }

    public Date getDate(){
        if(date==null){
            date = DateUtils.DateStringToDate(dateTimeStr);
        }
        return date;

    }

    public Calendar getCalendar(){
        if(calendar == null){
            calendar = DateUtils.DateStringToCalendar(dateTimeStr);
        }
        return calendar;
    }

    @Override
    public int compareTo(Game game) {
        return getDate().compareTo(game.getDate());
    }




}
