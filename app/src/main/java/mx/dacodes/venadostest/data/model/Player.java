package mx.dacodes.venadostest.data.model;

import com.google.gson.annotations.SerializedName;

import mx.dacodes.venadostest.utils.DateUtils;

public class Player {
    @SerializedName("name")
    private String name;

    @SerializedName("first_surname")
    private String firstSurname;

    @SerializedName("second_surname")
    private String secondSurname;

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("birth_place")
    private String birthPlace;

    @SerializedName("weight")
    private Double weight;

    @SerializedName("height")
    private Double height;

    @SerializedName("position")
    private String position;

    @SerializedName("role")
    private String role;

    @SerializedName("number")
    private Integer number;

    @SerializedName("position_short")
    private String positionShort;

    @SerializedName("role_short")
    private String roleShort;

    @SerializedName("last_team")
    private String lastTeam;

    @SerializedName("image")
    private String imageUrl;

    public Player() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionShort() {
        return positionShort;
    }

    public void setPositionShort(String positionShort) {
        this.positionShort = positionShort;
    }

    public String getLastTeam() {
        return lastTeam;
    }

    public void setLastTeam(String lastTeam) {
        this.lastTeam = lastTeam;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public String getRoleShort() {
        return roleShort;
    }

    public void setRoleShort(String roleShort) {
        this.roleShort = roleShort;
    }

    public String getBirthdayToDisplay(){
        return DateUtils.DateApiToApp(this.birthday);
    }
}
