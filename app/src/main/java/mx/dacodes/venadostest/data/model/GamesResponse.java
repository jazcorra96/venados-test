package mx.dacodes.venadostest.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GamesResponse {

    @SerializedName("success")
    private boolean success;

    @SerializedName("data")
    private DataGames data;

    public class DataGames{
        @SerializedName("games")
        private List<Game> games;

        @SerializedName("code")
        private int code;

        public List<Game> getGames() {
            return games;
        }

        public void setGames(List<Game> games) {
            this.games = games;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataGames getData() {
        return data;
    }

    public void setData(DataGames data) {
        this.data = data;
    }
}
