package mx.dacodes.venadostest.data.remote.services;

import mx.dacodes.venadostest.data.model.GamesResponse;
import mx.dacodes.venadostest.data.model.NotificationsResponse;
import mx.dacodes.venadostest.data.model.PlayersResponse;
import mx.dacodes.venadostest.data.model.SponsorsResponse;
import mx.dacodes.venadostest.data.model.StatisticsResponse;
import mx.dacodes.venadostest.utils.Constants;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiServiceInterface {

    @Headers({"Accept: application/json"})
    @GET(Constants.BASE_URL_STATISTICS)
    Call<StatisticsResponse> getStatistics();

    @Headers({"Accept: application/json"})
    @GET(Constants.BASE_URL_GAMES)
    Call<GamesResponse> getGames();

    @Headers({"Accept: application/json"})
    @GET(Constants.BASE_URL_PLAYERS)
    Call<PlayersResponse>getPlayers();

    @Headers({"Accept: application/json"})
    @GET(Constants.BASE_URL_SPONSORS)
    Call<SponsorsResponse>getSponsors();

    @Headers({"Accept: application/json"})
    @GET(Constants.BASE_URL_NOTIFICATIONS)
    Call<NotificationsResponse>getNotifications();
}
