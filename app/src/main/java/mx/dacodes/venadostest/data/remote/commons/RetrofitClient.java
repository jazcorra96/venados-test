package mx.dacodes.venadostest.data.remote.commons;

import java.util.concurrent.TimeUnit;

import mx.dacodes.venadostest.BuildConfig;
import mx.dacodes.venadostest.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String url){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);
            httpClient.readTimeout(Constants.TIME_READ_CONNECTION, TimeUnit.SECONDS);
            httpClient.connectTimeout(Constants.TIME_OUT_CONNECTION, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging);
        }

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(httpClient.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
