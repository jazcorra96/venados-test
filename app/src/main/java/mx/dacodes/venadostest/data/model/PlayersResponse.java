package mx.dacodes.venadostest.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlayersResponse {
    @SerializedName("success")
    private boolean success;

    @SerializedName("data")
    private DataPlayers data;

    public class DataPlayers{
        @SerializedName("team")
        private Team team;

        public class Team{
            @SerializedName("forwards")
            private List<Player> playersForward;

            @SerializedName("centers")
            private List<Player> playersCenter;

            @SerializedName("defenses")
            private List<Player> playersDefense;

            @SerializedName("goalkeepers")
            private List<Player> playersGoalkeeper;

            @SerializedName("coaches")
            private List<Player> playerCoach;

            public List<Player> getAllPlayers(){
                List<Player> playerList = new ArrayList<>();

                if(playersForward != null){ playerList.addAll(playersForward);}
                if(playersCenter != null){ playerList.addAll(playersCenter);}
                if(playersDefense != null){ playerList.addAll(playersDefense);}
                if(playersGoalkeeper != null){ playerList.addAll(playersGoalkeeper);}
                if(playerCoach != null){ playerList.addAll(playerCoach);}

                return playerList;
            }

            public List<Player> getPlayersForward() {
                return playersForward;
            }

            public void setPlayersForward(List<Player> playersForward) {
                this.playersForward = playersForward;
            }

            public List<Player> getPlayersCenter() {
                return playersCenter;
            }

            public void setPlayersCenter(List<Player> playersCenter) {
                this.playersCenter = playersCenter;
            }

            public List<Player> getPlayersDefense() {
                return playersDefense;
            }

            public void setPlayersDefense(List<Player> playersDefense) {
                this.playersDefense = playersDefense;
            }

            public List<Player> getPlayersGoalkeeper() {
                return playersGoalkeeper;
            }

            public void setPlayersGoalkeeper(List<Player> playersGoalkeeper) {
                this.playersGoalkeeper = playersGoalkeeper;
            }

            public List<Player> getPlayerCoach() {
                return playerCoach;
            }

            public void setPlayerCoach(List<Player> playerCoach) {
                this.playerCoach = playerCoach;
            }
        }

        @SerializedName("code")
        private int code;

        public Team getTeam() {
            return team;
        }

        public void setTeam(Team team) {
            this.team = team;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataPlayers getData() {
        return data;
    }

    public void setData(DataPlayers data) {
        this.data = data;
    }
}
