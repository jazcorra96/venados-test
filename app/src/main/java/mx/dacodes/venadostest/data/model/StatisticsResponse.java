package mx.dacodes.venadostest.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StatisticsResponse {

    @SerializedName("success")
    private boolean success;

    @SerializedName("data")
    private DataStatistics data;

    private class DataStatistics{
        @SerializedName("statistics")
        private List<Statistic> statistics;

        @SerializedName("code")
        private int code;

        public List<Statistic> getStatistics() {
            return statistics;
        }

        public void setStatistics(List<Statistic> statistics) {
            this.statistics = statistics;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataStatistics getData() {
        return data;
    }

    public void setData(DataStatistics data) {
        this.data = data;
    }
}
