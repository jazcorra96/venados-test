package mx.dacodes.venadostest.di.component;

import dagger.Component;
import mx.dacodes.venadostest.BaseApp;
import mx.dacodes.venadostest.di.module.ApplicationModule;

@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(BaseApp baseApp);
}
