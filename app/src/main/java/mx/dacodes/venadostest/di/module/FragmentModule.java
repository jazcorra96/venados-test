package mx.dacodes.venadostest.di.module;

import dagger.Module;
import dagger.Provides;
import mx.dacodes.venadostest.data.remote.commons.RetrofitClient;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.ui.home.HomeContract;
import mx.dacodes.venadostest.ui.home.HomePresenter;
import mx.dacodes.venadostest.ui.players.PlayersContract;
import mx.dacodes.venadostest.ui.players.PlayersPresenter;
import mx.dacodes.venadostest.ui.statistics.StatisticsContract;
import mx.dacodes.venadostest.ui.statistics.StatisticsPresenter;
import mx.dacodes.venadostest.utils.Constants;

@Module
public class FragmentModule {

    @Provides
    public HomeContract.Presenter provideHomePresenter(){
        return new HomePresenter();
    }

    @Provides
    public PlayersContract.Presenter providePlayersPresenter(){
        return new PlayersPresenter();
    }

    @Provides
    public StatisticsContract.Presenter provideStatisticsPresenter(){
        return new StatisticsPresenter();
    }

    @Provides
    public ApiServiceInterface provideApiService(){
        return RetrofitClient.getClient(Constants.BASE_URL).create(ApiServiceInterface.class);
    }
}
