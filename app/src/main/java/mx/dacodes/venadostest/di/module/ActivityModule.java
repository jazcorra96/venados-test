package mx.dacodes.venadostest.di.module;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import mx.dacodes.venadostest.ui.main.MainContract;
import mx.dacodes.venadostest.ui.main.MainPresenter;

@Module
public class ActivityModule {
    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity(){
        return activity;
    }

    @Provides
    MainContract.Presenter providePresenter(){
        return new MainPresenter();
    }
}
