package mx.dacodes.venadostest.di.component;

import dagger.Component;
import mx.dacodes.venadostest.di.module.ActivityModule;
import mx.dacodes.venadostest.ui.main.MainActivity;

@Component(modules = {ActivityModule.class})
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
}
