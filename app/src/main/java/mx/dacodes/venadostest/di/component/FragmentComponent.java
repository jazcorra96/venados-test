package mx.dacodes.venadostest.di.component;

import dagger.Component;
import mx.dacodes.venadostest.di.module.FragmentModule;
import mx.dacodes.venadostest.ui.home.HomeFragment;
import mx.dacodes.venadostest.ui.players.PlayersFragment;
import mx.dacodes.venadostest.ui.statistics.StatisticsFragment;

@Component(modules = {FragmentModule.class})
public interface FragmentComponent {
    void inject(HomeFragment homeFragment);
    void inject(PlayersFragment playersFragment);
    void inject(StatisticsFragment statisticsFragment);

}
