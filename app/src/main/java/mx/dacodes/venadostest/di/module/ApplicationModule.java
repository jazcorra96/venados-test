package mx.dacodes.venadostest.di.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import mx.dacodes.venadostest.BaseApp;
import mx.dacodes.venadostest.di.scope.PerApplication;
import timber.log.Timber;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(BaseApp baseApp) {
        this.application = baseApp;
    }

    @Provides
    @Singleton
    @PerApplication
    Application provideApplication(){
        return application;
    }

    @Provides
    Timber.DebugTree provideDebugTree(){
        return new Timber.DebugTree();
    }
}
