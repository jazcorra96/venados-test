package mx.dacodes.venadostest.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {
    public static String DateApiToApp(String value){
        if(value==null){
            value= "";
        }
        SimpleDateFormat inSDF = new SimpleDateFormat(Constants.FORMAT_API_DATE);
        Date date;
        String result;
        try {
            date = inSDF.parse(value);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            SimpleDateFormat outSDF = new SimpleDateFormat(Constants.FORMAT_APP_DATE);
            result = outSDF.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            result = "n/a";
        }

        return result;

    }

    public static Date DateStringToDate(String value){
        if(value==null){
            value= "";
        }
        SimpleDateFormat inSDF = new SimpleDateFormat(Constants.FORMAT_API_DATE);
        Date date = new Date();
        try {
            inSDF.setTimeZone(TimeZone.getDefault());
            date = inSDF.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static Calendar DateStringToCalendar(String value){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateStringToDate(value));
        return calendar;
    }

    public static String GetDayOfTheWeek(Calendar calendar){
        SimpleDateFormat inSDF = new SimpleDateFormat("EEE");
        return inSDF.format(calendar.getTime());
    }

    public static String GetMonthlyString(Date date){
        SimpleDateFormat inSDF = new SimpleDateFormat("MMMM yyyy");

        return inSDF.format(date);
    }

    public static Date GetMonthlyDate(String value){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat inSDF = new SimpleDateFormat("MMMM yyyy");
        Date date = new Date();
        try {
            date = inSDF.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
