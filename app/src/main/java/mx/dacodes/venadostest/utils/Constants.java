package mx.dacodes.venadostest.utils;

public class Constants {
    public static final long TIME_READ_CONNECTION = 20;
    public static final long TIME_OUT_CONNECTION = 20;
    public static final String BASE_URL = "https://venados.dacodes.mx";
    public static final String BASE_URL_STATISTICS = "/api/statistics";
    public static final String BASE_URL_GAMES = "/api/games";
    public static final String BASE_URL_PLAYERS = "/api/players";
    public static final String BASE_URL_SPONSORS = "/api/sponsors";
    public static final String BASE_URL_NOTIFICATIONS = "/api/notifications";

    public static final String FORMAT_API_DATE = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String FORMAT_APP_DATE = "dd / MM / yyyy";

    public static final String TEAM_NAME = "Venados F.C.";

}
