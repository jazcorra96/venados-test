package mx.dacodes.venadostest;

import android.app.Application;
import android.content.Context;

import javax.inject.Inject;

import dagger.Lazy;
import mx.dacodes.venadostest.di.component.ApplicationComponent;
import mx.dacodes.venadostest.di.component.DaggerApplicationComponent;
import mx.dacodes.venadostest.di.module.ApplicationModule;
import timber.log.Timber;


public class BaseApp extends Application {
    private static final BaseApp instance = new BaseApp();

    @Inject
    Lazy<Timber.DebugTree> debugTree;
    private ApplicationComponent applicationComponent;

    public static BaseApp getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        setup();

        if(BuildConfig.DEBUG){
            setupDebugTools();
        }

    }

    void setup(){
        applicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        applicationComponent.inject(this);
    }

    void setupDebugTools(){
        Timber.plant(debugTree.get());
    }

    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }

    public Context getApplicationContext(){
        return this;
    }
}
