package mx.dacodes.venadostest.ui.main;

import mx.dacodes.venadostest.ui.base.BaseContract;

public interface MainContract {
    interface View extends BaseContract.View{

    }

    interface Presenter extends BaseContract.Presenter<View>{

    }
}
