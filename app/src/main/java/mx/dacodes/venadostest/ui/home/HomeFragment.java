package mx.dacodes.venadostest.ui.home;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Game;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.di.component.DaggerFragmentComponent;
import mx.dacodes.venadostest.di.component.FragmentComponent;
import mx.dacodes.venadostest.di.module.FragmentModule;
import mx.dacodes.venadostest.ui.base.BaseFragment;
import mx.dacodes.venadostest.ui.home.adapters.PagerAdapter;
import mx.dacodes.venadostest.ui.main.MainActivity;

public class HomeFragment extends BaseFragment implements HomeContract.View{

    @BindView(R.id.viewpager_home)
    ViewPager viewPager;
    @BindView(R.id.tablayout_home)
    TabLayout tabLayout;
    @BindView(R.id.pb_general_loading)
    ProgressBar progressBar;

    @Inject
    HomeContract.Presenter presenter;

    @Inject
    ApiServiceInterface apiServiceInterface;

    private PagerAdapter pagerAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void injectDependency(){
        FragmentComponent fragmentComponent = DaggerFragmentComponent.builder().fragmentModule(new FragmentModule()).build();
        fragmentComponent.inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupTabWithViewPager();
        presenter.attachView(this);
        presenter.attachApi(apiServiceInterface);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        presenter.refreshGames();
    }

    private void setupTabWithViewPager(){
        if(pagerAdapter==null){
            pagerAdapter = new PagerAdapter(getChildFragmentManager(), getContext(), 0, new ArrayList<>(),new ArrayList<>());
        }
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void actionRefresh() {
        presenter.refreshGames();
    }


    @Override
    public void setDataFragmentPages(List<Game> data, String title) {
        pagerAdapter.addTab(data,title);
    }

    @Override
    public void showLoading(boolean show) {
        progressBar.setVisibility(show? View.VISIBLE:View.GONE);
        viewPager.setVisibility(show? View.GONE:View.VISIBLE);
    }
}
