package mx.dacodes.venadostest.ui.players;

import java.util.List;

import mx.dacodes.venadostest.data.model.Player;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.ui.base.BaseContract;
import mx.dacodes.venadostest.ui.home.HomeContract;

public interface PlayersContract {
    interface View extends BaseContract.View{
        void setPlayersData(List<Player> players);
        void showLoading(boolean show);
    }

    interface Presenter extends BaseContract.Presenter<View>{
        void attachApi(ApiServiceInterface apiServiceInterface);
        void refreshPlayers();
    }
}
