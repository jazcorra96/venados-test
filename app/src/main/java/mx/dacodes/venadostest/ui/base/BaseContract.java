package mx.dacodes.venadostest.ui.base;

public class BaseContract {

    public interface Presenter<T> {
        void attachView(T view);
    }

    public interface View{

    }
}
