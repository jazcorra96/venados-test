package mx.dacodes.venadostest.ui.statistics;

import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatisticsPresenter implements StatisticsContract.Presenter, Callback {

    private StatisticsContract.View view;
    private ApiServiceInterface apiServiceInterface;

    @Override
    public void attachView(StatisticsContract.View view) {
        this.view = view;
    }

    @Override
    public void onResponse(Call call, Response response) {

    }

    @Override
    public void onFailure(Call call, Throwable t) {

    }

    @Override
    public void attachApi(ApiServiceInterface apiServiceInterface) {
        this.apiServiceInterface = apiServiceInterface;
    }

    @Override
    public void refreshStatistics() {
        apiServiceInterface.getStatistics();
    }
}
