package mx.dacodes.venadostest.ui.main;

public class MainPresenter implements MainContract.Presenter{

    private MainContract.View view;

    @Override
    public void attachView(MainContract.View view) {
        this.view = view;
    }
}
