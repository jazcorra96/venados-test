package mx.dacodes.venadostest.ui.home;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mx.dacodes.venadostest.data.model.Game;
import mx.dacodes.venadostest.data.model.GamesResponse;
import mx.dacodes.venadostest.data.model.StatisticsResponse;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter implements HomeContract.Presenter, Callback {

    private HomeContract.View view;
    private ApiServiceInterface apiServiceInterface;
    private List<Game> games = new ArrayList<>();

    @Override
    public void attachView(HomeContract.View view) {
        this.view = view;
    }

    @Override
    public void refreshGames() {
        apiServiceInterface.getGames().enqueue(this);
    }

    @Override
    public void onResponse(Call call, Response response) {
        if(response.body() instanceof GamesResponse){
            GamesResponse gamesResponse = (GamesResponse) response.body();
            games = gamesResponse.getData().getGames();
            //TODO: Consider remove this pre-load or change it
            for (Game element : games){
                Picasso.get().load(element.getOpponentImgUrl());
            }

            updateDataFragmentPages();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        if(!games.isEmpty()){
            updateDataFragmentPages();
        }
        view.showLoading(false);
    }


    @Override
    public void attachApi(ApiServiceInterface apiServiceInterface) {
        this.apiServiceInterface = apiServiceInterface;
    }



    public List<Game> getLeagueGame(List<Game> allGames, String league){
        List<Game> leagueGames = new ArrayList<>();
        league = league.toLowerCase();
        for(Game game : allGames){
            if(game.getLeague().toLowerCase().equals(league)){
                leagueGames.add(game);
            }
        }
        return leagueGames;
    }

    public void updateDataFragmentPages(){
        List<String> tabs = getLeagueTitles(games);

        for(String title : tabs){
            view.setDataFragmentPages(getLeagueGame(games, title), title);
            view.showLoading(false);
        }

    }

    public List<String> getLeagueTitles(List<Game> data){
        List<String> titlesFound = new ArrayList<>();
        if(data!=null)
        for(Game game : data){
            if(!titlesFound.contains(game.getLeague())){
                titlesFound.add(game.getLeague());
            }
        }
        return titlesFound;
    }
}
