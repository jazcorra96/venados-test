package mx.dacodes.venadostest.ui.home.adapters;

import android.content.Intent;
import android.provider.CalendarContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Game;
import mx.dacodes.venadostest.utils.Constants;
import mx.dacodes.venadostest.utils.DateUtils;

public class GameRVHolderBody extends RecyclerView.ViewHolder {
    @BindView(R.id.game_holder_calendar)
    ImageView ivCalendar;

    @BindView(R.id.game_holder_day_number)
    TextView tvDayNumber;

    @BindView(R.id.game_holder_day_name)
    TextView tvDayName;

    @BindView(R.id.game_holder_home_img)
    ImageView ivHome;

    @BindView(R.id.game_holder_home_name)
    TextView tvHomeName;

    @BindView(R.id.game_holder_away_img)
    ImageView ivAway;

    @BindView(R.id.game_holder_away_name)
    TextView tvAwayName;

    @BindView(R.id.game_holder_score_home_away)
    TextView tvScoreHomeAway;

    @OnClick(R.id.btnCalendar)
    public void addEventToCalendar(){
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");

        String home = mGame.isLocal()? Constants.TEAM_NAME : mGame.getOpponent();
        String away = mGame.isLocal()? mGame.getOpponent() : Constants.TEAM_NAME;

        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, mGame.getCalendar().getTimeInMillis());
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, mGame.getCalendar().getTimeInMillis() + 60*60*1000);

        intent.putExtra(CalendarContract.Events.TITLE, "Partido: " + home + " vs " + away);
        itemView.getContext().startActivity(intent);
    }

    private Game mGame;

    Calendar calendar = Calendar.getInstance();

    public GameRVHolderBody(@NonNull View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    public void bindGame(Game game){
        mGame = game;
        calendar= DateUtils.DateStringToCalendar(game.getDateTimeStr());

        tvDayNumber.setText(String.valueOf(calendar.get(Calendar.DATE)));
        tvDayName.setText(DateUtils.GetDayOfTheWeek(calendar));

        TextView tvVenadosName = tvHomeName;
        TextView tvOpponentName = tvAwayName;
        ImageView ivVenados = ivHome;
        ImageView ivOpponent = ivAway;


        if(!game.isLocal()){
            tvVenadosName =  tvAwayName;
            tvOpponentName = tvHomeName;
            ivOpponent = ivHome;
            ivVenados = ivAway;
        }
        Picasso.get().load(game.getOpponentImgUrl()).resize(ivOpponent.getLayoutParams().width, ivOpponent.getLayoutParams().height).centerInside().into(ivOpponent);
        Picasso.get().load(R.drawable.venados_logo).resize(ivVenados.getLayoutParams().width, ivVenados.getLayoutParams().height).centerInside().into(ivVenados);

        tvVenadosName.setText(Constants.TEAM_NAME);
        tvOpponentName.setText(game.getOpponent());
        tvScoreHomeAway.setText(game.getHomeScore() + " - " + game.getAwayScore());


    }
}
