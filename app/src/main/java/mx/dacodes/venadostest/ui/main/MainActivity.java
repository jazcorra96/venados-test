package mx.dacodes.venadostest.ui.main;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.di.component.ActivityComponent;
import mx.dacodes.venadostest.di.component.DaggerActivityComponent;
import mx.dacodes.venadostest.di.module.ActivityModule;
import mx.dacodes.venadostest.ui.base.BaseActivity;
import mx.dacodes.venadostest.ui.home.HomeFragment;
import mx.dacodes.venadostest.ui.players.PlayersFragment;
import mx.dacodes.venadostest.ui.statistics.StatisticsFragment;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.mFragmentContainer)
    FrameLayout fragmentContainer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar_main)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,R.string.toggle_open, R.string.toggle_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(navigationView.getCheckedItem()==null){
            navigationView.setCheckedItem(R.id.menu_home);
        }
        onNavigationItemSelected(navigationView.getCheckedItem());

    }

    @Override
    protected void injectDependency() {
        ActivityComponent activityComponent = DaggerActivityComponent.builder().activityModule(new ActivityModule(this)).build();
        activityComponent.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    public void showHomeFragment(){
        toolbar.setVisibility(View.GONE);
        toolbar.setTitle(R.string.nav_title_home);
        getSupportFragmentManager().beginTransaction().replace(fragmentContainer.getId(), new HomeFragment()).commit();
    }

    public void showPlayersFragment(){
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle(R.string.nav_title_players);
        getSupportFragmentManager().beginTransaction().replace(fragmentContainer.getId(), new PlayersFragment()).commit();
    }

    public void showStatisticsFragment(){
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle(R.string.nav_title_statistics);
        getSupportFragmentManager().beginTransaction().replace(fragmentContainer.getId(), new StatisticsFragment()).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.menu_home:
                showHomeFragment();
                break;
            case R.id.menu_statistics:
                showStatisticsFragment();
                break;
            case R.id.menu_players:
                showPlayersFragment();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
