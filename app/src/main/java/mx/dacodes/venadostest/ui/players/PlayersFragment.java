package mx.dacodes.venadostest.ui.players;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Player;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.di.component.DaggerFragmentComponent;
import mx.dacodes.venadostest.di.component.FragmentComponent;
import mx.dacodes.venadostest.di.module.FragmentModule;
import mx.dacodes.venadostest.ui.base.BaseFragment;
import mx.dacodes.venadostest.ui.players.adapters.PlayersRVAdapter;

public class PlayersFragment extends BaseFragment implements PlayersContract.View {

    @BindView(R.id.rvPlayers)
    RecyclerView rvPlayers;
    @BindView(R.id.pb_players)
    ProgressBar pbPlayers;
    @Inject
    PlayersContract.Presenter presenter;
    @Inject
    ApiServiceInterface apiServiceInterface;

    private PlayersRVAdapter rvAdapter;

    @Override
    protected void injectDependency() {
        FragmentComponent fragmentComponent = DaggerFragmentComponent.builder().fragmentModule(new FragmentModule()).build();
        fragmentComponent.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_players;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.attachView(this);
        presenter.attachApi(apiServiceInterface);

        rvAdapter = new PlayersRVAdapter(getFragmentManager());
        rvPlayers.setLayoutManager(new GridLayoutManager(getContext(), 3, RecyclerView.VERTICAL, false));
        rvPlayers.setAdapter(rvAdapter);

        presenter.refreshPlayers();
    }

    @Override
    public void setPlayersData(List<Player> players) {
        rvAdapter.setData(players);
    }

    @Override
    public void showLoading(boolean show) {
        pbPlayers.setVisibility(show? View.VISIBLE : View.GONE);
    }
}
