package mx.dacodes.venadostest.ui.players;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Player;
import mx.dacodes.venadostest.utils.CircleTransformation;

public class PlayerDetailsFragment extends DialogFragment {
    @BindView(R.id.tv_player_fullname)
    TextView tvFullname;
    @BindView(R.id.tv_player_position)
    TextView tvPosition;
    @BindView(R.id.iv_player_profile)
    ImageView ivProfile;
    @BindView(R.id.tv_player_birthday)
    TextView tvBirthday;
    @BindView(R.id.tv_player_birthplace)
    TextView tvBirthplace;
    @BindView(R.id.tv_player_weight)
    TextView tvWeight;
    @BindView(R.id.tv_player_height)
    TextView tvHeight;
    @BindView(R.id.tv_player_number)
    TextView tvNumber;
    @BindView(R.id.tv_player_lastteam)
    TextView tvLastteam;
    private Player mPlayer;



    public PlayerDetailsFragment() {
    }

    public PlayerDetailsFragment(Player mPlayer) {
        this.mPlayer = mPlayer;
    }

    public static PlayerDetailsFragment newInstance(Player player){
        PlayerDetailsFragment fragment = new PlayerDetailsFragment(player);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_player_details, null);
        builder.setView(view);
        ButterKnife.bind(this, view);


        tvFullname.setText(mPlayer.getName() + " " + mPlayer.getFirstSurname() + " " + mPlayer.getSecondSurname());
        tvPosition.setText(mPlayer.getPosition()!=null? mPlayer.getPosition() : mPlayer.getRole());
        Picasso.get().load(mPlayer.getImageUrl()).transform(new CircleTransformation()).into(ivProfile);


        tvBirthday.setText(mPlayer.getBirthdayToDisplay());
        tvBirthplace.setText(mPlayer.getBirthPlace());
        tvWeight.setText((mPlayer.getWeight()==null? "-": mPlayer.getWeight()+ " kg") );
        tvHeight.setText((mPlayer.getHeight()==null? "-": mPlayer.getHeight()+ " m") );
        tvNumber.setText(mPlayer.getNumber()==null? "-": mPlayer.getNumber().toString());
        tvLastteam.setText(mPlayer.getLastTeam()==null? "No disponible": mPlayer.getLastTeam());


        builder.setCancelable(true);
        builder.setPositiveButton("Ok", null);
        return builder.create();
    }
}
