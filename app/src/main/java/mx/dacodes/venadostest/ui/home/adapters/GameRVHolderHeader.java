package mx.dacodes.venadostest.ui.home.adapters;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.dacodes.venadostest.R;

public class GameRVHolderHeader extends RecyclerView.ViewHolder {
    @BindView(R.id.tvHeader)
    TextView header;

    public GameRVHolderHeader(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindHeader(GameHeader header){
        this.header.setText(header.getTitle());
    }
}
