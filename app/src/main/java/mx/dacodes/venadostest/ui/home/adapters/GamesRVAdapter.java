package mx.dacodes.venadostest.ui.home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.saber.stickyheader.stickyView.StickHeaderRecyclerView;

import java.util.List;

import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Game;

public class GamesRVAdapter extends StickHeaderRecyclerView<Game, GameHeader> {

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType){
            case GameHeader.HEADER_TYPE_1:
                return new GameRVHolderHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_game_header, parent, false));
            default:
                return new GameRVHolderBody(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_game, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof GameRVHolderBody){
            ((GameRVHolderBody) holder).bindGame(getDataInPosition(position));
        }else if(holder instanceof GameRVHolderHeader){
            ((GameRVHolderHeader) holder).bindHeader(getHeaderDataInPosition(position));
        }
    }



    //@Override
    //public void onBindViewHolder(@NonNull GameRVHolderBody holder, int position) {
    //    holder.bindGame(data.get(position));
    //}


    //public void setData(List<Game> data){
    //    this.data = data;
    //}

    @Override
    public void bindHeaderData(View header, int headerPosition) {
        ((TextView)header.findViewById(R.id.tvHeader)).setText(getHeaderDataInPosition(headerPosition).getTitle());
    }
}
