package mx.dacodes.venadostest.ui.players.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Player;
import mx.dacodes.venadostest.ui.players.PlayerDetailsFragment;
import mx.dacodes.venadostest.utils.CircleTransformation;

public class PlayersRVHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.player_holder_profile)
    ImageView ivPlayerPortrait;

    @BindView(R.id.player_holder_position)
    TextView tvPlayerPosition;

    @BindView(R.id.player_holder_name)
    TextView tvPlayerName;

    @OnClick(R.id.player_holder_profile)
    public void showPlayerDetails(){
        DialogFragment detailsFragment = PlayerDetailsFragment.newInstance(player);
        detailsFragment.show(fragmentManager, player.getName());
    }

    private Player player;
    private FragmentManager fragmentManager;

    public PlayersRVHolder(@NonNull View itemView, FragmentManager fragmentManager) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.fragmentManager = fragmentManager;
    }

    public void bindPlayer(Player player){
        this.player = player;

        tvPlayerName.setText(player.getName());
        tvPlayerPosition.setText(player.getPosition());
        Picasso.get().load(player.getImageUrl()).transform(new CircleTransformation()).placeholder(R.drawable.ic_profile_black_100dp).into(ivPlayerPortrait);

    }
}
