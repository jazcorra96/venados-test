package mx.dacodes.venadostest.ui.home.adapters;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import mx.dacodes.venadostest.data.model.Game;
import mx.dacodes.venadostest.ui.home.HomeViewPageFragment;
import timber.log.Timber;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int pageCount;
    private List<String> tabTitles;
    private Context context;

    private List<HomeViewPageFragment> childFragments = new ArrayList<>();



    public PagerAdapter(FragmentManager fm, Context context, int pageCount, List<Game> data, List<String> tabTitles) {
        super(fm);
        for(String string : tabTitles){
            childFragments.add(new HomeViewPageFragment());
        }
        this.pageCount = pageCount;
        this.tabTitles = tabTitles;
        this.context = context;


    }

    public void addTab (List<Game> data, String tabTitle){
        if(!tabTitles.contains(tabTitle)){
            HomeViewPageFragment child = new HomeViewPageFragment();
            child.resetGameList();
            child.setGameList(data);
            childFragments.add(child);
            tabTitles.add(tabTitle);
            pageCount = childFragments.size();
            notifyDataSetChanged();
        }else{
            childFragments.get(tabTitles.indexOf(tabTitle)).resetGameList();
            childFragments.get(tabTitles.indexOf(tabTitle)).setGameList(data);

            notifyDataSetChanged();
        }

    }

    @Override
    public Fragment getItem(int position) {
        Timber.e("getItem(%d)", position);
        return childFragments.get(position);
    }

    @Override
    public int getCount() {
        return pageCount;
    }

    public void setFramentData(List<Game> data){
        for(HomeViewPageFragment child : childFragments){
            child.setGameList(data);
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(tabTitles.size()>0){
            return tabTitles.get(position);
        }
        return "";
    }

}
