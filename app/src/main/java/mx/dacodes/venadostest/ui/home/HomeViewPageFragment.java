package mx.dacodes.venadostest.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Game;
import mx.dacodes.venadostest.ui.home.adapters.GameHeader;
import mx.dacodes.venadostest.ui.home.adapters.GamesRVAdapter;
import mx.dacodes.venadostest.utils.DateUtils;

public class HomeViewPageFragment extends Fragment {

    @BindView(R.id.rvGames)
    RecyclerView rvGames;

    @BindView(R.id.swipeRefreshChildFragment)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<Game> gameList = new ArrayList<>();
    private List<GameHeader> gameHeaderList = new ArrayList<>();
    private GamesRVAdapter gamesRVAdapter;

    public HomeViewPageFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.child_fragment_home_page, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout.setOnRefreshListener(() -> ((HomeFragment)getParentFragment()).actionRefresh());
        rvGames.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        gamesRVAdapter = new GamesRVAdapter();
        rvGames.setAdapter(gamesRVAdapter);
        gamesRVAdapter.setHeaderAndData(new ArrayList<>(), null);
        setGameList(gameList);

    }

    public void resetGameList(){
        gameList.clear();
        gamesRVAdapter = new GamesRVAdapter();
    }

    public void setGameList(List<Game> games){
        gameList = games;
        gameHeaderList = getHeadersForGameList(games);
        if(isAdded()) {
            sortGamesByDate(gameList, false);

            if (!gameList.isEmpty())
                for (GameHeader gameHeader : gameHeaderList) {
                    gamesRVAdapter.setHeaderAndData(getMonthlyGames(gameList, gameHeader.getDate()), gameHeader);
                    gamesRVAdapter.notifyDataSetChanged();
                }
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public List<GameHeader> getHeadersForGameList(List<Game> games){
        List<GameHeader> headers = new ArrayList<>();
        List<String> titles = new ArrayList<>();
        for(Game game : games){
            if(!titles.contains(DateUtils.GetMonthlyString(game.getDate()))){
                titles.add(DateUtils.GetMonthlyString(game.getDate()));
                headers.add(new GameHeader(GameHeader.HEADER_TYPE_1, R.layout.holder_game_header, DateUtils.GetMonthlyString(game.getDate()),DateUtils.GetMonthlyDate(DateUtils.GetMonthlyString(game.getDate())) ));
            }
        }

        return headers;
    }

    public void sortGamesByDate(List<Game> games, boolean ascendingOrder){
        if(ascendingOrder){
            Collections.sort(games, Collections.reverseOrder());
        }else{
            Collections.sort(games);
        }

    }

    public List<Game> getMonthlyGames (List<Game> games, Date date){
        List<Game> monthlyGames = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        for(Game game : games){
            if(game.getDate().after(date) && game.getDate().before(calendar.getTime()) ){
                monthlyGames.add(game);
            }
        }

        return monthlyGames;
    }



}
