package mx.dacodes.venadostest.ui.home;

import android.view.View;

import java.util.List;

import mx.dacodes.venadostest.data.model.Game;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.ui.base.BaseContract;

public interface HomeContract {
    interface View extends BaseContract.View{
        void actionRefresh();
        void setDataFragmentPages(List<Game> data, String title);
        void showLoading(boolean show);
    }

    interface Presenter extends BaseContract.Presenter<View>{
        void attachApi(ApiServiceInterface apiServiceInterface);
        void refreshGames();
    }
}
