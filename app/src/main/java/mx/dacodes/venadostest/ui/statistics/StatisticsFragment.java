package mx.dacodes.venadostest.ui.statistics;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.di.component.DaggerFragmentComponent;
import mx.dacodes.venadostest.di.component.FragmentComponent;
import mx.dacodes.venadostest.di.module.FragmentModule;
import mx.dacodes.venadostest.ui.base.BaseFragment;

public class StatisticsFragment extends BaseFragment implements StatisticsContract.View {

    @Inject
    StatisticsContract.Presenter presenter;
    @Inject
    ApiServiceInterface apiServiceInterface;

    @Override
    protected void injectDependency() {
        FragmentComponent fragmentComponent = DaggerFragmentComponent.builder().fragmentModule(new FragmentModule()).build();
        fragmentComponent.inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_statistics;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
