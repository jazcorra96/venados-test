package mx.dacodes.venadostest.ui.home.adapters;

import androidx.annotation.LayoutRes;

import com.saber.stickyheader.stickyData.HeaderData;

import java.util.Date;

public class GameHeader implements HeaderData {
    public static final int HEADER_TYPE_1 = 1;

    private int headerType;
    @LayoutRes
    private final int layoutResource;
    private String title;
    private Date date;

    public GameHeader(int headerType, int layoutResource, String title, Date date) {
        this.headerType = headerType;
        this.layoutResource = layoutResource;
        this.title = title;
        this.date = date;
    }

    @Override
    public int getHeaderLayout() {
        return layoutResource;
    }

    @Override
    public int getHeaderType() {
        return headerType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
