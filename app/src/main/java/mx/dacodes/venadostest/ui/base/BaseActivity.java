package mx.dacodes.venadostest.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependency();
        setContentView(getLayoutId());
        ButterKnife.bind(this);
    }
    protected abstract void injectDependency();
    protected abstract int getLayoutId();
}
