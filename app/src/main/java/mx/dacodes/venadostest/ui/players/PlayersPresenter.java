package mx.dacodes.venadostest.ui.players;

import java.util.ArrayList;
import java.util.List;

import mx.dacodes.venadostest.data.model.Player;
import mx.dacodes.venadostest.data.model.PlayersResponse;
import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.ui.home.HomeContract;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayersPresenter implements PlayersContract.Presenter, Callback {

    private PlayersContract.View view;
    private ApiServiceInterface apiServiceInterface;
    private List<Player> mPlayers = new ArrayList<>();

    @Override
    public void attachView(PlayersContract.View view) {
        this.view = view;
    }

    @Override
    public void onResponse(Call call, Response response) {
        if(response.body() instanceof PlayersResponse){
            PlayersResponse playersResponse = (PlayersResponse) response.body();
            mPlayers = playersResponse.getData().getTeam().getAllPlayers();
            view.setPlayersData(mPlayers);
            view.showLoading(false);
        }

    }

    @Override
    public void onFailure(Call call, Throwable t) {
        view.showLoading(false);
    }

    @Override
    public void attachApi(ApiServiceInterface apiServiceInterface) {
        this.apiServiceInterface = apiServiceInterface;
    }

    @Override
    public void refreshPlayers() {
        apiServiceInterface.getPlayers().enqueue(this);
        view.showLoading(true);
    }
}
