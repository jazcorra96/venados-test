package mx.dacodes.venadostest.ui.players.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import mx.dacodes.venadostest.R;
import mx.dacodes.venadostest.data.model.Player;

public class PlayersRVAdapter extends RecyclerView.Adapter<PlayersRVHolder> {

    private List<Player> data = new ArrayList<>();
    private FragmentManager fragmentManager;
    public PlayersRVAdapter(FragmentManager fm) {
        this.fragmentManager = fm;
    }

    public PlayersRVAdapter(List<Player> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public PlayersRVHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return  new PlayersRVHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_player, parent, false), fragmentManager);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayersRVHolder holder, int position) {
        holder.bindPlayer(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Player> data){
        this.data = data;
        notifyDataSetChanged();
    }
}
