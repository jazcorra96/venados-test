package mx.dacodes.venadostest.ui.statistics;

import mx.dacodes.venadostest.data.remote.services.ApiServiceInterface;
import mx.dacodes.venadostest.ui.base.BaseContract;
import mx.dacodes.venadostest.ui.home.HomeContract;

public interface StatisticsContract {
    interface View extends BaseContract.View{

    }

    interface Presenter extends BaseContract.Presenter<View>{
        void attachApi(ApiServiceInterface apiServiceInterface);
        void refreshStatistics();
    }
}
